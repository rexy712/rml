/**
	This file is a part of rexy's math library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rml/mat.hpp"
#include "rml/vec.hpp"
#include "rml/quat.hpp"

namespace rml{
	template class matrix<float,2,2>;
	template class matrix<float,3,3>;
	template class matrix<float,4,4>;

	template class matrix<int,2,2>;
	template class matrix<int,3,3>;
	template class matrix<int,4,4>;

	template class matrix<double,2,2>;
	template class matrix<double,3,3>;
	template class matrix<double,4,4>;

	template class vector<float,2>;
	template class vector<float,3>;
	template class vector<float,4>;

	template class vector<int,2>;
	template class vector<int,3>;
	template class vector<int,4>;

	template class vector<double,2>;
	template class vector<double,3>;
	template class vector<double,4>;

	template class quaternion<float>;
	template class quaternion<int>;
	template class quaternion<double>;
}
