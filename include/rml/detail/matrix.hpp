/**
	This file is a part of rexy's math library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RML_DETAIL_MATRIX_HPP
#define RML_DETAIL_MATRIX_HPP

#include <cstdlib> //size_t
#include <utility> //integer_sequence
#include "../fwd_declare.hpp"

namespace rml::detail{

	template<size_t SW, size_t W = SW, size_t H = SW-1, size_t... Args>
	struct gen_id_tup {
		using tup = typename gen_id_tup<SW, W-1, H, Args..., 0>::tup;
	};
	template<size_t SW, size_t H, size_t... Args>
	struct gen_id_tup<SW,SW,H,Args...> {
		using tup = typename gen_id_tup<SW, SW-1, H, Args..., 1>::tup;
	};
	template<size_t SW, size_t H, size_t... Args>
	struct gen_id_tup<SW,0,H,Args...> {
		using tup = typename gen_id_tup<SW, SW, H-1, Args..., 0>::tup;
	};
	template<size_t SW, size_t... Args>
	struct gen_id_tup<SW,SW,0,Args...> {
		using tup = std::integer_sequence<size_t,Args...,1>;
	};


	template<size_t N, size_t... Args>
	struct gen_zero_tup {
		using tup = typename gen_zero_tup<N-1,Args...,0>::tup;
	};
	template<size_t... Args>
	struct gen_zero_tup<0,Args...> {
		using tup = std::integer_sequence<size_t,Args...>;
	};

	template<size_t W>
	struct id_initialization_matrix {
		using tuple = typename gen_id_tup<W>::tup;
	};


	template<size_t W, size_t H>
	struct default_initialization_matrix {
		using tuple = typename gen_zero_tup<W>::tup;
	};
	template<size_t W>
	struct default_initialization_matrix<W,W> {
		using tuple = typename id_initialization_matrix<W>::tuple;
	};

	template<Scalar T, size_t R>
	class mat_ref_obj
	{
	public:
		using size_type = size_t;

	protected:
		T* m_data = nullptr;
	public:
		constexpr mat_ref_obj(T* d, size_type i);
		constexpr T& operator[](size_type i);
		constexpr const T& operator[](size_type i)const;
	};
	template<Scalar T, size_t R>
	struct determinate_helper {
		static constexpr T perform(const matrix<T,R,R>& m);
	};
	template<Scalar T>
	struct determinate_helper<T,3> {
		static constexpr T perform(const matrix<T,3,3>& m);
	};
	template<Scalar T>
	struct determinate_helper<T,2> {
		static constexpr T perform(const matrix<T,2,2>& m);
	};

	template<Scalar T, size_t R>
	struct inverse_helper {
		//TODO generalized inverse
	};
	template<Scalar T>
	struct inverse_helper<T,2> {
		static constexpr matrix<T,2,2> perform(const matrix<T,2,2>& m);
	};
	template<Scalar T>
	struct inverse_helper<T,3> {
		static constexpr matrix<T,3,3> perform(const matrix<T,3,3>& m);
	};
	template<Scalar T>
	struct inverse_helper<T,4> {
		static constexpr matrix<T,4,4> perform(const matrix<T,4,4>& m);
	};

}

#include "matrix.tpp"

#endif
