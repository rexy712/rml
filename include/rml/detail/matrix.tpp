/**
	This file is a part of rexy's math library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RML_DETAIL_MATRIX_TPP
#define RML_DETAIL_MATRIX_TPP

#include <cstdlib> //size_t
#include <utility> //integer_sequence

namespace rml::detail{

	template<Scalar T, size_t R>
	constexpr mat_ref_obj<T,R>::mat_ref_obj(T* d, size_type i):
		m_data(d+(i*R)){}

	template<Scalar T, size_t R>
	constexpr T& mat_ref_obj<T,R>::operator[](size_type i){
		return m_data[i];
	}
	template<Scalar T, size_t R>
	constexpr const T& mat_ref_obj<T,R>::operator[](size_type i)const{
		return m_data[i];
	}
	template<Scalar T, size_t R>
	constexpr T determinate_helper<T,R>::perform(const matrix<T,R,R>& m){
		T sum = 0;
		T op = 1;
		for(size_t i = 0; i < R; ++i){
			T item = op * m[0][i];
			matrix<T,R-1,R-1> mul(no_initialize);
			for(size_t j = 1, mj = 0; j < R; ++j){
				for(size_t k = 0, mk = 0; k < R; ++k){
					if(k == i)
						continue;
					mul[mj][mk] = m[j][k];
					++mk;
				}
				++mj;
			}
			sum += item * determinate_helper<T,R-1>::perform(mul);
			op = -op;
		}
		return sum;
	}
	template<Scalar T>
	constexpr T determinate_helper<T,3>::perform(const matrix<T,3,3>& m){
		return (m.get(0) * ((m.get(4) * m.get(8)) - (m.get(5) * m.get(7))) -
		        m.get(1) * ((m.get(3) * m.get(8)) - (m.get(5) * m.get(6))) +
		        m.get(2) * ((m.get(3) * m.get(7)) - (m.get(4) * m.get(6))));
	}
	template<Scalar T>
	constexpr T determinate_helper<T,2>::perform(const matrix<T,2,2>& m){
		return m.get(0) * m.get(3) - m.get(1) * m.get(2);
	}

	template<Scalar T>
	constexpr matrix<T,2,2> inverse_helper<T,2>::perform(const matrix<T,2,2>& m){
		T det = m.determinate();
		if(!det)
			return matrix<T,2,2>(zero_initialize);
		return matrix<T,2,2>(m.get(3) / det, -(m.get(1)) / det, -(m.get(2)) / det, m.get(0) / det);
	}
	template<Scalar T>
	constexpr matrix<T,3,3> inverse_helper<T,3>::perform(const matrix<T,3,3>& m){
		T det = m.determinate();
		if(!det)
			return matrix<T,3,3>(zero_initialize);
		return matrix<T,3,3>(((m.get(4) * m.get(8)) - (m.get(5) * m.get(7))) / det,
		                    -((m.get(1) * m.get(8)) - (m.get(2) * m.get(7))) / det,
		                     ((m.get(1) * m.get(5)) - (m.get(2) * m.get(4))) / det,
		                    -((m.get(3) * m.get(8)) - (m.get(5) * m.get(6))) / det,
		                     ((m.get(0) * m.get(8)) - (m.get(2) * m.get(6))) / det,
		                    -((m.get(0) * m.get(5)) - (m.get(2) * m.get(3))) / det,
		                     ((m.get(3) * m.get(7)) - (m.get(4) * m.get(6))) / det,
		                    -((m.get(0) * m.get(7)) - (m.get(1) * m.get(6))) / det,
		                     ((m.get(0) * m.get(4)) - (m.get(1) * m.get(3))) / det);
	}
	template<Scalar T>
	constexpr matrix<T,4,4> inverse_helper<T,4>::perform(const matrix<T,4,4>& m){
		//barely over 50 lines, can be made slightly shorter by making the return statement unreadable
		T det = m.determinate();
		if(!det)
			return matrix<T,4,4>(zero_initialize);
		return matrix<T,4,4>((m.get(5) * ((m.get(10) * m.get(15)) - (m.get(11) * m.get(14)))  -
		                      m.get(6) * ((m.get(9)  * m.get(15)) - (m.get(11) * m.get(13)))  +
		                      m.get(7) * ((m.get(9)  * m.get(14)) - (m.get(10) * m.get(13)))) / det,
		                    -(m.get(1) * ((m.get(10) * m.get(15)) - (m.get(11) * m.get(14)))  -
		                      m.get(2) * ((m.get(9)  * m.get(15)) - (m.get(11) * m.get(13)))  +
		                      m.get(3) * ((m.get(9)  * m.get(14)) - (m.get(10) * m.get(13)))) / det,
		                     (m.get(1) * ((m.get(6)  * m.get(15)) - (m.get(7)  * m.get(14)))  -
		                      m.get(2) * ((m.get(5)  * m.get(15)) - (m.get(7)  * m.get(13)))  +
		                      m.get(3) * ((m.get(5)  * m.get(14)) - (m.get(6)  * m.get(13)))) / det,
		                    -(m.get(1) * ((m.get(6)  * m.get(11)) - (m.get(7)  * m.get(10)))  -
		                      m.get(2) * ((m.get(5)  * m.get(11)) - (m.get(7)  * m.get(9)))   +
		                      m.get(3) * ((m.get(5)  * m.get(10)) - (m.get(6)  * m.get(9))))  / det,
		                    -(m.get(4) * ((m.get(10) * m.get(15)) - (m.get(11) * m.get(14)))  -
		                      m.get(6) * ((m.get(8)  * m.get(15)) - (m.get(11) * m.get(12)))  +
		                      m.get(7) * ((m.get(8)  * m.get(14)) - (m.get(10) * m.get(12)))) / det,
		                     (m.get(0) * ((m.get(10) * m.get(15)) - (m.get(11) * m.get(14)))  -
		                      m.get(2) * ((m.get(8)  * m.get(15)) - (m.get(11) * m.get(12)))  +
		                      m.get(3) * ((m.get(8)  * m.get(14)) - (m.get(10) * m.get(12)))) / det,
		                    -(m.get(0) * ((m.get(6)  * m.get(15)) - (m.get(7)  * m.get(14)))  -
		                      m.get(2) * ((m.get(4)  * m.get(15)) - (m.get(7)  * m.get(12)))  +
		                      m.get(3) * ((m.get(4)  * m.get(14)) - (m.get(6)  * m.get(12)))) / det,
		                     (m.get(0) * ((m.get(6)  * m.get(11)) - (m.get(7)  * m.get(10)))  -
		                      m.get(2) * ((m.get(4)  * m.get(11)) - (m.get(7)  * m.get(8)))   +
		                      m.get(3) * ((m.get(4)  * m.get(10)) - (m.get(6)  * m.get(8))))  / det,
		                     (m.get(4) * ((m.get(9)  * m.get(15)) - (m.get(11) * m.get(13)))  -
		                      m.get(5) * ((m.get(8)  * m.get(15)) - (m.get(11) * m.get(12)))  +
		                      m.get(7) * ((m.get(8)  * m.get(13)) - (m.get(9)  * m.get(12)))) / det,
		                    -(m.get(0) * ((m.get(9)  * m.get(15)) - (m.get(11) * m.get(13)))  -
		                      m.get(1) * ((m.get(8)  * m.get(15)) - (m.get(11) * m.get(12)))  +
		                      m.get(3) * ((m.get(8)  * m.get(13)) - (m.get(9)  * m.get(12)))) / det,
		                     (m.get(0) * ((m.get(5)  * m.get(15)) - (m.get(7)  * m.get(13)))  -
		                      m.get(1) * ((m.get(4)  * m.get(15)) - (m.get(7)  * m.get(12)))  +
		                      m.get(3) * ((m.get(4)  * m.get(13)) - (m.get(5)  * m.get(12)))) / det,
		                    -(m.get(0) * ((m.get(5)  * m.get(11)) - (m.get(7)  * m.get(9)))   -
		                      m.get(1) * ((m.get(4)  * m.get(11)) - (m.get(7)  * m.get(8)))   +
		                      m.get(3) * ((m.get(4)  * m.get(9))  - (m.get(5)  * m.get(8))))  / det,
		                    -(m.get(4) * ((m.get(9)  * m.get(14)) - (m.get(10) * m.get(13)))  -
		                      m.get(5) * ((m.get(8)  * m.get(14)) - (m.get(10) * m.get(12)))  +
		                      m.get(6) * ((m.get(8)  * m.get(13)) - (m.get(9)  * m.get(12)))) / det,
		                     (m.get(0) * ((m.get(9)  * m.get(14)) - (m.get(10) * m.get(13)))  -
		                      m.get(1) * ((m.get(8)  * m.get(14)) - (m.get(10) * m.get(12)))  +
		                      m.get(2) * ((m.get(8)  * m.get(13)) - (m.get(9)  * m.get(12)))) / det,
		                    -(m.get(0) * ((m.get(5)  * m.get(14)) - (m.get(6)  * m.get(13)))  -
		                      m.get(1) * ((m.get(4)  * m.get(14)) - (m.get(6)  * m.get(12)))  +
		                      m.get(2) * ((m.get(4)  * m.get(13)) - (m.get(5)  * m.get(12)))) / det,
		                     (m.get(0) * ((m.get(5)  * m.get(10)) - (m.get(6)  * m.get(9)))   -
		                      m.get(1) * ((m.get(4)  * m.get(10)) - (m.get(6)  * m.get(8)))   +
		                      m.get(2) * ((m.get(4)  * m.get(9))  - (m.get(5)  * m.get(8))))  / det);
	}

}

#endif
