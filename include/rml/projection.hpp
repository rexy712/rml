/**
	This file is a part of rexy's math library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RML_PROJECTION_HPP
#define RML_PROJECTION_HPP

#include "mat.hpp"
#include "vec.hpp"
#include "math_common.hpp"

namespace rml{

	template<Scalar T>
	matrix<T,4,4> fov_projection(T fov, T asp, T near, T far);
	template<Scalar T>
	matrix<T,4,4> fov_asymetric_projection(T fovl, T fovr, T fovb, T fovt, T asp, T near, T far);
	template<Scalar T>
	matrix<T,4,4> ortho_projection(T w, T h, T n, T f);
	template<Scalar T>
	matrix<T,4,4> ortho_asymetric_projection(T l, T r, T b, T t, T n, T f);

	template<Scalar T>
	vec3<T> project(const mat4<T>& viewproj_mat, const vec3<T>& world_coords, const vec4<T>& viewport);
	template<Scalar T>
	vec3<T> unproject(const mat4<T>& viewproj_mat, const vec3<T>& viewport_coords, const vec4<T>& viewport);

}

#include "projection.tpp"

#endif
