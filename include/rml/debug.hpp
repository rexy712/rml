/**
	This file is a part of rexy's math library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RML_DEBUG_HPP
#define RML_DEBUG_HPP

#include <cstdio> //printf
#include <cstdlib> //size_t

#include "quat.hpp"
#include "mat.hpp"

namespace rml{
	namespace detail{
		static inline void print_integral(int i){
			printf("%d", i);
		}
		static inline void print_integral(float f){
			printf("%f", f);
		}
		static inline void print_double(double d){
			printf("%lf", d);
		}
		static inline void print_integral(unsigned int i){
			printf("%u", i);
		}
		static inline void print_integral(long int i){
			printf("%li", i);
		}
		static inline void print_integral(unsigned long int i){
			printf("%lu", i);
		}
		static inline void print_integral(long double d){
			printf("%Lf", d);
		}
	}

	//Debug
	template<Scalar T, size_t R, size_t C>
	void dump_matrix(const matrix_base<T,R,C>& mat){
		for(size_t i = 0;i < C;++i){
			for(size_t j = 0;j < R;++j){
				detail::print_integral(mat[i][j]);
				printf(" ");
			}
			printf("\n");
		}
		printf("\n");
	}
	template<Scalar T>
	void dump_quaternion(const quaternion<T>& q){
		for(size_t i = 0;i < 4;++i){
			detail::print_integral(q[i]);
			printf(" ");
		}
		printf("\n");
	}
}

#endif
