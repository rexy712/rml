/**
	This file is a part of rexy's math library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RML_VEC_TPP
#define RML_VEC_TPP

#include <cmath> //sqrt

namespace rml{

	template<Scalar T, size_t R>
	template<size_t TR,Scalar... Args,std::enable_if_t<TR <= R && (std::is_convertible_v<Args,T> && ...),int>>
	constexpr vector<T,R>::vector(const vector<T,TR>& other, Args&&... args){
		static_assert(sizeof...(args) + TR <= R);
		size_type i = 0;
		for(;i < TR;++i){
			this->m_data[i] = other[i];
		}
		if constexpr(sizeof...(args) > 0){
			assign_(i, std::forward<Args>(args)...);
		}
	}
	template<Scalar T, size_t R>
	template<Scalar U>
	constexpr vector<T,R>::vector(const vector<U,R>& other){
		for(size_type i = 0;i < R;++i){
			this->m_data[i] = other[i];
		}
	}
	template<Scalar T, size_t R>
	template<Scalar U, Scalar... Args>
	constexpr void vector<T,R>::assign_(size_type offset, U&& u, Args&&... args){
		this->m_data[offset] = std::forward<U>(u);
		if constexpr(sizeof...(args) > 0){
			assign_(offset + 1, std::forward<Args>(args)...);
		}
	}
	template<Scalar T, size_t R>
	template<Scalar U, size_t TR>
	constexpr vector<T,R>& vector<T,R>::operator=(const vector<U,TR>& m){
		base::operator=(m);
		return *this;
	}
	template<Scalar T, size_t R>
	constexpr auto vector<T,R>::operator[](size_type i) -> reference{
		return this->m_data[i];
	}
	template<Scalar T, size_t R>
	constexpr auto vector<T,R>::operator[](size_type i)const -> const_reference{
		return this->m_data[i];
	}

	template<Scalar T, size_t R>
	constexpr auto vector<T,R>::x(void) -> reference{
		return this->m_data[0];
	}
	template<Scalar T, size_t R>
	constexpr auto vector<T,R>::x(void)const -> const_reference{
		return this->m_data[0];
	}
	template<Scalar T, size_t R>
	template<Scalar U>
	constexpr auto vector<T,R>::y(void) -> reference{
		static_assert(R > 1, "Vector does not contain a 2nd element");
		return this->m_data[1];
	}
	template<Scalar T, size_t R>
	template<Scalar U>
	constexpr auto vector<T,R>::y(void)const -> const_reference{
		static_assert(R > 1, "Vector does not contain a 2nd element");
		return this->m_data[1];
	}
	template<Scalar T, size_t R>
	template<Scalar U>
	constexpr auto vector<T,R>::z(void) -> reference{
		static_assert(R > 2, "Vector does not contain a 3rd element");
		return this->m_data[2];
	}
	template<Scalar T, size_t R>
	template<Scalar U>
	constexpr auto vector<T,R>::z(void)const -> const_reference{
		static_assert(R > 2, "Vector does not contain a 3rd element");
		return this->m_data[2];
	}
	template<Scalar T, size_t R>
	template<Scalar U>
	constexpr auto vector<T,R>::w(void) -> reference{
		static_assert(R > 3, "Vector does not contain a 4th element");
		return this->m_data[3];
	}
	template<Scalar T, size_t R>
	template<Scalar U>
	constexpr auto vector<T,R>::w(void)const -> const_reference{
		static_assert(R > 3, "Vector does not contain a 4th element");
		return this->m_data[3];
	}
	template<Scalar T, size_t R>
	auto vector<T,R>::magnitude(void)const -> value_type{
		value_type sum = 0;
		for(size_type i = 0;i < R;++i){
			sum += (this->m_data[i] * this->m_data[i]);
		}
		return sqrt(sum);
	}
	template<Scalar T, size_t R>
	vector<T,R> vector<T,R>::normalize(void){
		return (*this) / magnitude();
	}

	template<Scalar T>
	constexpr auto perp(const vector<T,2>& v){
		return vec2<T>(-v[1], v[0]);
	}
	template<Scalar T, Scalar U, size_t R1, size_t R2>
	constexpr auto perp(const vector<T,R1>& left, const vector<U,R2>& right){
		return (left[0] * right[1]) - (left[1] * right[0]);
	}
	template<Scalar T, Scalar U>
	constexpr auto cross(const vector<T,3>& left, const vector<U,3>& right){
		using res_t = decltype(left[0] * right[0]);
		return vec3<res_t>((left[1] * right[2]) - (left[2] * right[1]),
	                     (left[2] * right[0]) - (left[0] * right[2]),
	                     (left[0] * right[1]) - (left[1] * right[0]));
	}
	template<Scalar T, size_t R>
	constexpr auto magnitude(const vector<T,R>& v){
		return v.magnitude();
	}

	template<Scalar T, Scalar U, size_t C, size_t R>
	constexpr auto operator*(const matrix<U,R,C>& left, const vector<T,C>& right){
		using res_t = decltype(std::declval<T>() * std::declval<U>());
		vector<res_t,R> res(zero_initialize);
		size_t index = 0;
		//columns == rows
		for(size_t i = 0; i < R; ++i){
			for(size_t k = 0; k < C; ++k){
				res.get(index) += left.get(k, i) * right[k];
			}
			++index;
		}
		return res;
	}
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator*(const vector<T,R>& left, const vector<U,R>& right){
		using res_t = decltype(std::declval<T>() * std::declval<U>());
		res_t res = 0;
		for(size_t i = 0; i < R; ++i){
			res += left[i] * right[i];
		}
		return res;
	}
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator*(const vector<T,R>& left, U&& right){
		using res_t = decltype(std::declval<T>() * std::declval<U>());
		vector<res_t,R> res(zero_initialize);
		for(size_t i = 0; i < R; ++i){
			res[i] = left[i] * std::forward<U>(right);
		}
		return res;
	}
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator*(U&& left, const vector<T,R>& right){
		using res_t = decltype(std::declval<U>() * std::declval<T>());
		vector<res_t,R> res(zero_initialize);
		for(size_t i = 0; i < R; ++i){
			res[i] = std::forward<U>(left) * right[i];
		}
		return res;
	}
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator/(const vector<T,R>& left, U&& right){
		using res_t = decltype(std::declval<T>() / std::declval<U>());
		vector<res_t,R> res(zero_initialize);
		for(size_t i = 0; i < R; ++i){
			res[i] = left[i] / std::forward<U>(right);
		}
		return res;
	}
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator+(const vector<T,R>& left, const vector<U,R>& right){
		using res_t = decltype(std::declval<T>() + std::declval<U>());
		vector<res_t,R> res(zero_initialize);
		for(size_t i = 0; i < R; ++i){
			res[i] = left[i] + right[i];
		}
		return res;
	}
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator-(const vector<T,R>& left, const vector<U,R>& right){
		using res_t = decltype(std::declval<T>() - std::declval<U>());
		vector<res_t,R> res(zero_initialize);
		for(size_t i = 0; i < R; ++i){
			res[i] = left[i] - right[i];
		}
		return res;
	}
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator-(const vector<T,R>& left){
		using res_t = decltype(-std::declval<U>());
		vector<res_t,R> res(zero_initialize);
		for(size_t i = 0; i < R; ++i){
			res[i] = -left[i];
		}
		return res;
	}

	template<Scalar T, Scalar U, size_t R>
	constexpr decltype(auto) operator*=(vector<T,R>& left, U&& right){
		for(size_t i = 0; i < R; ++i){
			left[i] *= right;
		}
		return left;
	}
	template<Scalar T, Scalar U, size_t R>
	constexpr decltype(auto) operator/=(vector<T,R>& left, U&& right){
		for(size_t i = 0; i < R; ++i){
			left[i] /= right;
		}
		return left;
	}
	template<Scalar T, Scalar U, size_t R>
	constexpr decltype(auto) operator+=(vector<T,R>& left, const vector<U,R>& right){
		for(size_t i = 0; i < R; ++i){
			left[i] += right[i];
		}
		return left;
	}
	template<Scalar T, Scalar U, size_t R>
	constexpr decltype(auto) operator-=(vector<T,R>& left, const vector<U,R>& right){
		for(size_t i = 0; i < R; ++i){
			left[i] -= right[i];
		}
		return left;
	}

}

#endif
