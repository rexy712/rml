/**
	This file is a part of rexy's math library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RML_MAT_HPP
#define RML_MAT_HPP

#include <cstdlib> //size_t
#include <utility> //integer_sequence
#include <type_traits> //decay_t, is_same, integral_constant
#include "math_common.hpp"
#include "fwd_declare.hpp"

#include "rml.hpp"

namespace rml{

	//Common stuff shared by all types of matrices
	template<Scalar T, size_t R, size_t C>
	class matrix_base
	{
		static_assert(C > 0, "Cannot have 0 columns matrix");
		static_assert(R > 0, "Cannot have 0 rows matrix");
	public:
		using value_type = T;
		using size_type = size_t;
		using pointer = value_type*;
		using const_pointer = const value_type*;
		using reference = value_type&;
		using const_reference = const value_type&;

		static constexpr size_type Columns = C;
		static constexpr size_type Rows = R;

	protected:
		value_type m_data[R*C];

	protected:
    template<size_t... Ss>
    constexpr matrix_base(std::integer_sequence<size_type,Ss...>);
	public:
		//Default construct as identity when square, zero otherwise
		constexpr matrix_base(void);

		//Range initializing constructors
		constexpr explicit matrix_base(detail::zero_initialize_t);
		constexpr explicit matrix_base(detail::no_initialize_t);

		//Value initializing constructors
		constexpr explicit matrix_base(value_type v);
		template<Scalar... Args, std::enable_if_t<(std::is_convertible_v<Args,T> && ...),int> = 0>
		constexpr matrix_base(Args&&... args);

		//Copying constructors
		constexpr matrix_base(const matrix_base&) = default;
		constexpr matrix_base(matrix_base&&) = default;
		template<Scalar U>
		constexpr matrix_base(const matrix_base<U,Columns,Rows>& m);
		~matrix_base(void) = default;

		constexpr matrix_base& operator=(const matrix_base&) = default;
		constexpr matrix_base& operator=(matrix_base&&) = default;
		template<Scalar U, size_t TR, size_t TC>
		constexpr matrix_base& operator=(const matrix_base<U,TR,TC>& m);

		//Getters/Setters
		constexpr auto operator[](size_type x);
		constexpr auto operator[](size_type x)const;
		constexpr reference get(size_type x, size_type y);
		constexpr const_reference get(size_type x, size_type y)const;
		constexpr reference get(size_type i);
		constexpr const_reference get(size_type i)const;

		constexpr size_type columns(void)const;
		constexpr size_type rows(void)const;
		constexpr size_type size(void)const;

		constexpr pointer raw(void);
		constexpr const_pointer raw(void)const;
		constexpr operator pointer(void);
		constexpr operator const_pointer(void)const;
	};

	//Non square matrices
	template<Scalar T, size_t R, size_t C>
	class matrix : public matrix_base<T,R,C>
	{
	private:
		using base = matrix_base<T,R,C>;
	public:
		using value_type = typename base::value_type;
		using size_type = typename base::size_type;
		using pointer = typename base::pointer;
		using const_pointer = typename base::const_pointer;
		using reference = typename base::reference;
		using const_reference = typename base::const_reference;

	public:
		using base::base;

		template<size_t TR, size_t TC, std::enable_if_t<TR <= R && TC <= C,int> = 0>
		constexpr matrix(const matrix_base<value_type,TR,TC>& other);
		template<Scalar U>
		constexpr matrix(const matrix<U,R,C>& other);
		constexpr matrix(const matrix&) = default;
		constexpr matrix(matrix&&) = default;
		~matrix(void) = default;

		//Assignement
		constexpr matrix& operator=(const matrix&) = default;
		constexpr matrix& operator=(matrix&&) = default;
		template<Scalar U>
		constexpr matrix& operator=(const matrix<U,R,C>& m);
	};

	//Square matrices
	template<Scalar T, size_t R>
	class matrix<T,R,R> : public matrix_base<T,R,R>
	{
	private:
		using base = matrix_base<T,R,R>;
	public:
		using value_type = typename base::value_type;
		using size_type = typename base::size_type;
		using pointer = typename base::pointer;
		using const_pointer = typename base::const_pointer;
		using reference = typename base::reference;
		using const_reference = typename base::const_reference;

	public:
		using base::base;

		constexpr matrix(const matrix&) = default;
		constexpr matrix(matrix&&) = default;
		constexpr explicit matrix(detail::id_initialize_t);
		template<size_t TR, size_t TC, std::enable_if_t<TR <= R && TC <= R,int> = 0>
		constexpr matrix(const matrix_base<value_type,TR,TC>& other);
		template<Scalar U>
		constexpr matrix(const matrix<U,R,R>& other);
		~matrix(void) = default;

		//Assignement
		constexpr matrix& operator=(const matrix&) = default;
		constexpr matrix& operator=(matrix&&) = default;
		template<Scalar U>
		constexpr matrix& operator=(const matrix<U,R,R>& m);

		//square matrix arithmetic operations
		constexpr value_type determinate(void)const;
		constexpr value_type trace(void)const;
		constexpr matrix transpose(void)const;
		constexpr matrix inverse(void)const;
	};

	template<Scalar T, size_t R>
	constexpr T determinate(const matrix<T,R,R>& m);
	template<Scalar T, size_t R>
	constexpr matrix<T,R,R> inverse(const matrix<T,R,R>& m);

	template<Scalar T>
	matrix<T,2,2> rotation2d_pure(T angle);
	template<Scalar T>
	constexpr matrix<T,2,2> rotation2d_pure(T sin, T cos);
	template<Scalar T>
	constexpr matrix<T,2,2> scale2d(T x, T y);

	template<Scalar T>
	matrix<T,3,3> rotation2d(T angle);
	template<Scalar T>
	constexpr matrix<T,3,3> rotation2d(T sin, T cos);
	template<Scalar T>
	matrix<T,3,3> rotation2d(T x, T y, T z);

	template<Scalar T>
	constexpr matrix<T,4,4> rotation3d(T angle_x, T angle_y, T angle_z);
	template<Scalar T>
	constexpr matrix<T,4,4> translation3d(T x, T y, T z);
	template<Scalar T>
	constexpr matrix<T,4,4> scale3d(T x, T y, T z);

	//Logic operators
	template<Scalar T, Scalar U, size_t R, size_t C>
	constexpr bool operator==(const matrix_base<T,R,C>& left, const matrix_base<U,R,C> right);
	template<Scalar T, Scalar U, size_t R, size_t C>
	constexpr bool operator!=(const matrix_base<T,R,C>& left, const matrix_base<U,R,C> right);

	//Arithmetic operators
	template<Scalar T, Scalar U, size_t R1, size_t C1, size_t R2>
	constexpr auto operator*(const matrix<T,R1,C1>& left, const matrix<U,C1,R2>& right);
	template<Scalar T, Scalar U, size_t C, size_t R>
	constexpr auto operator*(const matrix<T,R,C>& left, U&& right);
	template<Scalar T, Scalar U, size_t C, size_t R>
	constexpr auto operator*(U&& left, const matrix<T,R,C>& right);
	template<Scalar T, Scalar U, size_t C, size_t R>
	constexpr auto operator/(const matrix<T,R,C>& left, U&& right);
	template<Scalar T, Scalar U, size_t C, size_t R>
	constexpr auto operator+(const matrix<T,R,C>& left, const matrix<U,R,C>& right);
	template<Scalar T, Scalar U, size_t C, size_t R>
	constexpr auto operator-(const matrix<T,R,C>& left, const matrix<U,R,C>& right);
	template<Scalar T, Scalar U, size_t C, size_t R>
	constexpr auto operator-(const matrix<T,R,C>& left);

	template<Scalar T, size_t R, size_t C>
	constexpr auto abs(const matrix_base<T,R,C>& left);
	template<Scalar T, Scalar U, Scalar V, size_t R, size_t C>
	constexpr bool fuzzy_eq(const matrix_base<T,R,C>& left, const matrix_base<U,R,C>& right, const V& epsilon);
	template<Scalar T, Scalar U, Scalar V, size_t R, size_t C>
	constexpr bool fuzzy_neq(const matrix_base<T,R,C>& left, const matrix_base<U,R,C>& right, const V& epsilon);

	//Arithmetic assignment operators
	template<Scalar T, Scalar U, size_t R>
	constexpr decltype(auto) operator*=(matrix<T,R,R>& left, const matrix<U,R,R>& right);
	template<Scalar T, Scalar U, size_t C, size_t R>
	constexpr decltype(auto) operator*=(matrix<T,R,C>& left, U&& right);
	template<Scalar T, Scalar U, size_t C, size_t R>
	constexpr decltype(auto) operator/=(matrix<T,R,C>& left, U&& right);
	template<Scalar T, Scalar U, size_t C, size_t R>
	constexpr decltype(auto) operator+=(matrix<T,R,C>& left, const matrix<U,R,C>& right);
	template<Scalar T, Scalar U, size_t C, size_t R>
	constexpr decltype(auto) operator-=(matrix<T,R,C>& left, const matrix<U,R,C>& right);

#if RML_INSTANTIATIONS_ENABLED
	extern template class matrix<float,2,2>;
	extern template class matrix<float,3,3>;
	extern template class matrix<float,4,4>;

	extern template class matrix<int,2,2>;
	extern template class matrix<int,3,3>;
	extern template class matrix<int,4,4>;

	extern template class matrix<double,2,2>;
	extern template class matrix<double,3,3>;
	extern template class matrix<double,4,4>;
#endif

}

#include "mat.tpp"

#endif
