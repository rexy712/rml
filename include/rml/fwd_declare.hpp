/**
	This file is a part of rexy's math library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RML_FWD_DECLARE_HPP
#define RML_FWD_DECLARE_HPP

#include <cstdlib> //size_t
#include <type_traits>
#ifdef __cpp_lib_concepts
#include <concepts>
namespace detail{
	template<class T, class U>
	concept convertible_to = std::convertible_to<T,U>;
}
#else
namespace detail{
	template<class T, class U>
	concept convertible_to = std::is_convertible_v<T,U>;
}
#endif

//Provide aliases for common matrix, vector, and quaternion types

namespace rml{

	//Must forward declare type traits for use in concepts because you can't forward declare concepts
	template<class... Ms>
	struct is_vector;
	template<class... Qs>
	struct is_quaternion;
	template<class... Ms>
	struct is_matrix;

	//Create concepts that depend on the type traits
	template<class T>
	concept Quaternion = is_quaternion<T>::value;
	template<class T>
	concept Matrix = is_matrix<T>::value;
	template<class T>
	concept Vector = is_vector<T>::value;

	template<class T>
	concept Scalar = !Matrix<T> && !Vector<T> && !Quaternion<T> && requires(std::decay_t<T> t){
		{t += t} -> detail::convertible_to<T>;
		{t -= t} -> detail::convertible_to<T>;
		{t /= t} -> detail::convertible_to<T>;
		{t *= t} -> detail::convertible_to<T>;
		{t + t}  -> detail::convertible_to<std::decay_t<T>>;
		{t - t}  -> detail::convertible_to<std::decay_t<T>>;
		{t / t}  -> detail::convertible_to<std::decay_t<T>>;
		{t * t}  -> detail::convertible_to<std::decay_t<T>>;
		{-t}     -> detail::convertible_to<std::decay_t<T>>;
		{t > t}  -> detail::convertible_to<bool>;
		{t < t}  -> detail::convertible_to<bool>;
		{t >= t} -> detail::convertible_to<bool>;
		{t <= t} -> detail::convertible_to<bool>;
		{t == t} -> detail::convertible_to<bool>;
		{t != t} -> detail::convertible_to<bool>;
	};

	template<Scalar T, size_t R, size_t C>
	class matrix_base;

	template<Scalar T, size_t R, size_t C>
	class matrix;
	template<Scalar T, size_t R>
	class vector;
	template<Scalar T>
	class quaternion;

	template<Scalar T>
	using mat2 = matrix<T,2,2>;
	template<Scalar T>
	using mat3 = matrix<T,3,3>;
	template<Scalar T>
	using mat4 = matrix<T,4,4>;

	template<Scalar T>
	using vec2 = vector<T,2>;
	template<Scalar T>
	using vec3 = vector<T,3>;
	template<Scalar T>
	using vec4 = vector<T,4>;

	using vec2f = vec2<float>;
	using vec2i = vec2<int>;
	using vec2u = vec2<unsigned int>;
	using vec2d = vec2<double>;
	using vec2s = vec2<size_t>;
	using vec2b = vec2<int>;

	using vec3f = vec3<float>;
	using vec3i = vec3<int>;
	using vec3u = vec3<unsigned int>;
	using vec3d = vec3<double>;
	using vec3s = vec3<size_t>;
	using vec3b = vec3<int>;

	using vec4f = vec4<float>;
	using vec4i = vec4<int>;
	using vec4u = vec4<unsigned int>;
	using vec4d = vec4<double>;
	using vec4s = vec4<size_t>;
	using vec4b = vec4<int>;

	using mat2f = mat2<float>;
	using mat2i = mat2<int>;
	using mat2u = mat2<unsigned int>;
	using mat2d = mat2<double>;
	using mat2s = mat2<size_t>;
	using mat2b = mat2<int>;

	using mat3f = mat3<float>;
	using mat3i = mat3<int>;
	using mat3u = mat3<unsigned int>;
	using mat3d = mat3<double>;
	using mat3s = mat3<size_t>;

	using mat4f = mat4<float>;
	using mat4i = mat4<int>;
	using mat4u = mat4<unsigned int>;
	using mat4d = mat4<double>;
	using mat4s = mat4<size_t>;
	using mat4b = mat4<int>;

	template<Scalar T>
	using quat = quaternion<T>;

	using quat_f = quat<float>;
	using quat_i = quat<int>;
	using quat_u = quat<unsigned int>;
	using quat_d = quat<double>;
	using quat_s = quat<size_t>;
	using quat_b = quat<int>;

	namespace detail{

		template<class T>
		struct is_matrix_helper {
			template<class U, size_t R, size_t C>
			static std::true_type test(matrix_base<U,R,C>*);
			static std::false_type test(void*);

			static constexpr bool value = std::is_same<std::true_type,decltype(test(static_cast<std::decay_t<T>*>(nullptr)))>::value;
		};

		template<class T>
		struct is_quat_helper {
			template<class U>
			static std::true_type test(quaternion<U>*);
			static std::false_type test(void*);
			static constexpr bool value = std::is_same<std::true_type,decltype(test(static_cast<std::decay_t<T>*>(nullptr)))>::value;
		};

		template<class T>
		struct is_vector_helper {
			template<class U, size_t R>
			static std::true_type test(vector<U,R>*);
			static std::false_type test(void*);

			static constexpr bool value = std::is_same<std::true_type,decltype(test(static_cast<std::decay_t<T>*>(nullptr)))>::value;
		};

	}

	template<class... Ms>
	struct is_vector{
		static constexpr bool value = (detail::is_vector_helper<Ms>::value && ...);
	};
	template<class... Qs>
	struct is_quaternion {
		static constexpr bool value = (detail::is_quat_helper<Qs>::value && ...);
	};
	template<class... Ms>
	struct is_matrix {
		static constexpr bool value = (detail::is_matrix_helper<Ms>::value && ...);
	};

}

#endif
