/**
	This file is a part of rexy's math library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RML_PROJECTION_TPP
#define RML_PROJECTION_TPP

#include "mat.hpp"
#include "vec.hpp"
#include <cmath> //sin, cos, tan

namespace rml{

	template<Scalar T>
	matrix<T,4,4> fov_projection(T fov, T asp, T near, T far){
		T r = near * std::tan(fov / T{2.0});
		return matrix<T,4,4>((near / r) / asp, T{0},          T{0},                               T{0},
		                     T{0},             (near / r),    T{0},                               T{0},
		                     T{0},             T{0},          (far + near) / (near - far),       -T{1},
		                     T{0},             T{0},          (T{2} * near * far) / (near - far), T{0});
	}
	template<Scalar T>
	matrix<T,4,4> fov_asymetric_projection(T fovl, T fovr, T fovb, T fovt, T asp, T n, T f){
		T l = n * std::tan(fovl);
		T r = n * std::tan(fovr);
		T b = n * std::tan(fovb);
		T t = n * std::tan(fovt);

		return matrix<T,4,4>(((T{2} * n) / (r - l)) * asp,   T{0},                 T{0},                     T{0},
		                       T{0},                         (T{2} * n) / (t - b), T{0},                     T{0},
		                       (r + l) / (r - l),            (t + b) / (t - b),    (f + n) / (n - f),       -T{1},
		                       T{0},                         T{0},                 (T{2} * n * f) / (n - f), T{0});
	}
	template<Scalar T>
	matrix<T,4,4> ortho_projection(T w, T h, T n, T f){
		return matrix<T,4,4>(T{2} / w, T{0},     T{0},                 T{0},
		                     T{0},     T{2} / h, T{0},                 T{0},
		                     T{0},     T{0},     T{1} / (n - f),       T{0},
		                     T{0},     T{0},     (n + f) / (n - f),    T{1});
	}
	template<Scalar T>
	matrix<T,4,4> ortho_asymetric_projection(T l, T r, T b, T t, T n, T f){
		return matrix<T,4,4>(T{2} / (r - l),       T{0},                 T{0},                 T{0},
		                     T{0},                 T{2} / (t - b),       T{0},                 T{0},
		                     T{0},                 T{0},                 T{1} / (n - f),       T{0},
		                     (r + l) / (l - r),    (t + b) / (b - t),    (n + f) / (n - f),    T{1});
	}

	template<Scalar T>
	vec3<T> project(const mat4<T>& viewproj_mat, const vec3<T>& w_coords, const vec4<T>& viewport){
		//project world coordinates to ndc coordinates
		vec4<T> world_coords{w_coords[0], w_coords[1], w_coords[2], T{1.0}};
		vec4<T> ndc_coords = viewproj_mat * world_coords;

		//perspective_division
		ndc_coords /= ndc_coords[3];

		//project ndc coordinates to viewport coordinates
		return vec3<T>{((ndc_coords[0] + T{1.0}) * viewport[2] * T{0.5}) + viewport[0],
		               ((ndc_coords[1] + T{1.0}) * viewport[3] * T{0.5}) + viewport[1],
		               ndc_coords[2]
		};
	}
	template<Scalar T>
	vec3<T> unproject(const mat4<T>& viewproj_mat, const vec3<T>& viewport_coords, const vec4<T>& viewport){
		//project viewport coordinates to ndc coordinates
		vec4<T> ndc_coords{((viewport_coords[0] - viewport[0]) * T{2.0} / viewport[2]) - T{1.0},
		                  ((viewport_coords[1] - viewport[1]) * T{2.0} / viewport[3]) - T{1.0},
		                  viewport_coords[2],
		                  T{1.0}};

		//project ndc coordinates to world coordinates
		mat4<T> inv_viewproj_mat = viewproj_mat.inverse();
		vec4<T> world_coords = inv_viewproj_mat * ndc_coords;

		//perspective division
		world_coords /= world_coords[3];

		return {world_coords[0], world_coords[1], world_coords[2]};
	}

}

#endif
