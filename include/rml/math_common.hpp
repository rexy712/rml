/**
	This file is a part of rexy's math library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RML_DETAIL_MATH_HPP
#define RML_DETAIL_MATH_HPP

#include "fwd_declare.hpp"

namespace rml{

	namespace detail{
		//sentinel classes saying how to initialize some math classes
		struct zero_initialize_t {};
		struct no_initialize_t {};
		struct id_initialize_t {};
		struct manual_initialize_t {};
	}

	//instantiation of sentinels
	static inline constexpr detail::zero_initialize_t zero_initialize;
	static inline constexpr detail::no_initialize_t no_initialize;
	static inline constexpr detail::id_initialize_t id_initialize;
	static inline constexpr detail::manual_initialize_t manual_initialize;

	template<Scalar T>
	static constexpr T pi(void){
		return static_cast<T>(3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821);
	}
	template<Scalar T>
	static constexpr T to_degrees(T t){
		return (t * 180.0) / pi<T>();
	}
	template<Scalar T>
	static constexpr T to_radians(T t){
		return (t * pi<T>()) / 180.0;
	}

	template<Scalar T>
	static constexpr T clamp(const T& t, const T& min, const T& max){
		if(t < min)
			return min;
		if(t > max)
			return max;
		return t;
	}
	template<Scalar T>
	static constexpr T clamp_min(const T& t, const T& min){
		if(t < min)
			return min;
		return t;
	}
	template<Scalar T>
	static constexpr T clamp_max(const T& t, const T& max){
		if(t > max)
			return max;
		return t;
	}

}
constexpr long double operator"" _rad(long double f){
	return f;
}
constexpr long double operator"" _deg(long double f){
	return rml::to_radians(f);
}


#endif
