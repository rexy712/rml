/**
	This file is a part of rexy's math library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RML_VEC_HPP
#define RML_VEC_HPP

#include <cstdlib> //size_t
#include <type_traits> //enable_if, convertible_to

#include "mat.hpp"
#include "math_common.hpp"

#include "rml.hpp"

namespace rml{

	//class representing vectors
	//inherit from matrix base because it also shared matrix attributes
	template<Scalar T, size_t R>
	class vector : public matrix_base<T,R,1>
	{
	private:
		using base = matrix_base<T,R,1>;
	public:
		using value_type = typename base::value_type;
		using size_type = typename base::size_type;
		using pointer = typename base::pointer;
		using const_pointer = typename base::const_pointer;
		using reference = typename base::reference;
		using const_reference = typename base::const_reference;

	public:
		using base::base;

		template<size_t TR,Scalar... Args,std::enable_if_t<TR <= R && (std::is_convertible_v<Args,T> && ...),int> = 0>
		constexpr vector(const vector<T,TR>& other, Args&&... args);
		template<Scalar U>
		constexpr vector(const vector<U,R>& other);
		constexpr vector(const vector&) = default;
		constexpr vector(vector&&) = default;
		~vector(void) = default;

		//Assignement
		constexpr vector& operator=(const vector&) = default;
		constexpr vector& operator=(vector&&) = default;
		template<Scalar U, size_t TR>
		constexpr vector& operator=(const vector<U,TR>& m);

		constexpr reference operator[](size_type i);
		constexpr const_reference operator[](size_type i)const;

		constexpr reference x(void);
		constexpr const_reference x(void)const;
		template<Scalar U = T>
		constexpr reference y(void);
		template<Scalar U = T>
		constexpr const_reference y(void)const;
		template<Scalar U = T>
		constexpr reference z(void);
		template<Scalar U = T>
		constexpr const_reference z(void)const;
		template<Scalar U = T>
		constexpr reference w(void);
		template<Scalar U = T>
		constexpr const_reference w(void)const;

		value_type magnitude(void)const;
		vector normalize(void);
	protected:
		template<Scalar U, Scalar... Args>
		constexpr void assign_(size_type offset, U&& u, Args&&... args);
	};

	template<Scalar T>
	constexpr auto perp(const vector<T,2>& v);
	template<Scalar T, Scalar U, size_t R1, size_t R2>
	constexpr auto perp(const vector<T,R1>& left, const vector<U,R2>& right);
	template<Scalar T, Scalar U>
	constexpr auto cross(const vector<T,3>& left, const vector<U,3>& right);

	template<Scalar T, size_t R>
	constexpr auto magnitude(const vector<T,R>& v);

	template<Scalar T, Scalar U, size_t C, size_t R>
	constexpr auto operator*(const matrix<U,R,C>& left, const vector<T,C>& right);
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator*(const vector<T,R>& left, const vector<U,R>& right);
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator*(const vector<T,R>& left, U&& right);
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator*(U&& left, const vector<T,R>& right);
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator/(const vector<T,R>& left, U&& right);
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator+(const vector<T,R>& left, const vector<U,R>& right);
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator-(const vector<T,R>& left, const vector<U,R>& right);
	template<Scalar T, Scalar U, size_t R>
	constexpr auto operator-(const vector<T,R>& left);

	template<Scalar T, Scalar U, size_t R>
	constexpr decltype(auto) operator*=(vector<T,R>& left, U&& right);
	template<Scalar T, Scalar U, size_t R>
	constexpr decltype(auto) operator/=(vector<T,R>& left, U&& right);
	template<Scalar T, Scalar U, size_t R>
	constexpr decltype(auto) operator+=(vector<T,R>& left, const vector<U,R>& right);
	template<Scalar T, Scalar U, size_t R>
	constexpr decltype(auto) operator-=(vector<T,R>& left, const vector<U,R>& right);

#if RML_INSTANTIATIONS_ENABLED
	extern template class vector<float,2>;
	extern template class vector<float,3>;
	extern template class vector<float,4>;

	extern template class vector<int,2>;
	extern template class vector<int,3>;
	extern template class vector<int,4>;

	extern template class vector<double,2>;
	extern template class vector<double,3>;
	extern template class vector<double,4>;
#endif

}

#include "vec.tpp"

#endif
